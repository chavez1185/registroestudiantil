﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EjemploMVC.Migrations
{
    public partial class migracion_inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Mantenimientos");

            migrationBuilder.CreateTable(
                name: "tblAsignaturas",
                schema: "Mantenimientos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblAsignaturas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tblGeneros",
                schema: "Mantenimientos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblGeneros", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tblGrados",
                schema: "Mantenimientos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false),
                    Acronimo = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblGrados", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tblSecciones",
                schema: "Mantenimientos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblSecciones", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblAsignaturas",
                schema: "Mantenimientos");

            migrationBuilder.DropTable(
                name: "tblGeneros",
                schema: "Mantenimientos");

            migrationBuilder.DropTable(
                name: "tblGrados",
                schema: "Mantenimientos");

            migrationBuilder.DropTable(
                name: "tblSecciones",
                schema: "Mantenimientos");
        }
    }
}
