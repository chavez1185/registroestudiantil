using Microsoft.EntityFrameworkCore;

using Models.Mantenimientos;

public class RegistroEstudiantilContext:DbContext{
    public RegistroEstudiantilContext(DbContextOptions<RegistroEstudiantilContext> options):base(options){
        {
            Database.Migrate();
        }
    }
    
    //Mantenimientos
    public DbSet<Genero> Generos{get;set;}
    public DbSet<Grado> Grado{get;set;}
    public DbSet<Seccion> Secciones{get;set;}
    public DbSet<Asignatura> Asignatura{get;set;}
}