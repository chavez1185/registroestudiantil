﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



    public class Matricula
    {
    [Key]
    public int Id { get; set; }

    [ForeignKey("Planificacion")]
    public int PlanificacionId { get; set; }

    [ForeignKey("Estudiante")]
    public int EstudianteId { get; set; }

    public float NotaFinal { get; set; }
}

