using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Mantenimientos
{
    [Table("tblGeneros", Schema = "Mantenimientos")]
    public class Genero {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Nombre{get;set;}
    }    
}