using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Mantenimientos
{
    [Table("tblAsignaturas", Schema="Mantenimientos")]
    public class Asignatura {
        [Key]
        public int Id { get; set; }

        [Required ]
        [StringLength (100)]
        public string Nombre { get; set; }
    }
}