using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Mantenimientos
{
    [Table("tblGrados", Schema = "Mantenimientos")]

    public class Grado{
        public Grado():this(""){}
        public Grado(string nombre):this(0,nombre){}
        public Grado(int id, string nombre){
            Id = id;
            Nombre = nombre;
        }

        [Key]
        public int Id{get;set;}

        [Required]
        [StringLength(50)]
        public string Nombre{get;set;}

        [Required]
        [StringLength(50)]
        public string Acronimo {get; set;}
    }
}