using Models.Mantenimientos;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class Profesor{
    [Key]
    public int Id { get; set; }

    [Required]
    [StringLength(20)]
    public int Identidad { get; set; }

    [Required]
    [StringLength(50)]
    public string Nombres { get; set; }

    [Required]
    [StringLength(50)]
    public string Apellidos { get; set; }

    [Required]
    [DataType(DataType.DateTime)]
    public DateTime FechaNacimiento { get; set; }

    [ForeignKey("Genero")]
    public int GeneroId { get; set; }

    public virtual Genero Genero { get; set; }
}